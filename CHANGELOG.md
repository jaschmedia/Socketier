# Socketier-0.1.0

Initial development/conversion from java.

#### Added
* License statement and related things
* Changelogs (yes, this is somewhat meta)
* Message Types and related utility functions
* Logging functionality
* Connections (WIP)
* Connections: connection ID system in place
* Connections: protocol system (server) in place
* Connections: event handling system

#### Changed

#### Fixed

#### Removed