<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>me.Jasch.Socketier</groupId>
    <artifactId>Socketier</artifactId>
    <packaging>jar</packaging>
    <version>0.1.0</version>
    <name>Socketier</name>
    <description>A kotlin wrapper around WebSocket for easier, event driven server-client communication.</description>
    <url>https://gitlab.com/jaschmedia/Socketier</url>

    <prerequisites>
        <maven>3.2.0</maven>
    </prerequisites>

    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>https://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
            <comments>A business-friendly OSS license</comments>
        </license>
    </licenses>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <kotlin.version>1.1.1</kotlin.version>
        <spek.version>1.1.0-beta2</spek.version>
        <junit.platform.version>1.0.0-M3</junit.platform.version>
        <jupiter.version>5.0.0-M3</jupiter.version>
        <dokka.version>0.9.13</dokka.version>
        <main.class>me.Jasch.Socketier.Socketier</main.class>
    </properties>

    <dependencies>
        <!-- KOTLIN -->
        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-stdlib</artifactId>
            <version>${kotlin.version}</version>
        </dependency>
        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-reflect</artifactId>
            <version>${kotlin.version}</version>
        </dependency>

        <!-- JUnit tests -->
        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-commons</artifactId>
            <version>${junit.platform.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-launcher</artifactId>
            <version>${junit.platform.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>${jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-runner</artifactId>
            <version>${junit.platform.version}</version>
            <scope>test</scope>
        </dependency>

        <!-- SPEK testing -->
        <dependency>
            <groupId>org.jetbrains.spek</groupId>
            <artifactId>spek-api</artifactId>
            <version>${spek.version}</version>
            <scope>test</scope>
            <!-- <type>pom</type> -->
        </dependency>
        <dependency>
            <groupId>org.jetbrains.spek</groupId>
            <artifactId>spek-junit-platform-engine</artifactId>
            <version>${spek.version}</version>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.jetbrains.spek</groupId>
                    <artifactId>spek-api</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <!-- SPEK assertion library -->
        <dependency>
            <groupId>com.winterbe</groupId>
            <artifactId>expekt</artifactId>
            <version>0.5.0</version>
            <scope>test</scope>
        </dependency>

        <!-- Logging -->
        <dependency> <!-- requires slf4j -->
            <groupId>io.github.microutils</groupId>
            <artifactId>kotlin-logging</artifactId>
            <version>1.4.4</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.25</version>
        </dependency>
        <dependency> <!-- logging framework used by slf4j -->
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.2.2</version>
        </dependency>

        <!-- WebSocket implementation to be used -->
        <dependency>
            <groupId>org.java-websocket</groupId>
            <artifactId>Java-WebSocket</artifactId>
            <version>1.3.0</version>
        </dependency>

        <!-- Used for generating random alphanumeric strings. -->
        <dependency>
            <groupId>commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
            <version>2.6</version>
        </dependency>
    </dependencies>

    <!-- Who is working on this project. -->
    <developers>
        <developer>
            <id>jasch</id>
            <name>Jan S.</name>
            <url>https://gitlab.com/jaschmedia/</url>
            <timezone>+1</timezone>
            <roles>
                <role>founder</role>
                <role>maintainer</role>
            </roles>
        </developer>
    </developers>

    <pluginRepositories>
        <pluginRepository>
            <id>jcenter</id>
            <name>JCenter</name>
            <url>https://jcenter.bintray.com/</url>
        </pluginRepository>
    </pluginRepositories>

    <build>
        <sourceDirectory>${project.basedir}/src/main/kotlin</sourceDirectory>
        <testSourceDirectory>${project.basedir}/src/test/kotlin</testSourceDirectory>
        <plugins>
            <!-- compiler -->
            <plugin>
                <artifactId>kotlin-maven-plugin</artifactId>
                <groupId>org.jetbrains.kotlin</groupId>
                <version>${kotlin.version}</version>

                <executions>
                    <execution>
                        <id>compile</id>
                        <goals> <goal>compile</goal> </goals>
                    </execution>

                    <execution>
                        <id>test-compile</id>
                        <goals> <goal>test-compile</goal> </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>3.0.2</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <mainClass>${main.class}</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>

            <!-- Create a shaded jar -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>3.0.0</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <finalName>${project.artifactId}-${project.version}-SHADED</finalName>
                            <transformers>
                                <transformer
                                        implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                    <mainClass>${main.class}</mainClass>
                                </transformer>
                            </transformers>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- Attaching source -->
            <plugin>
                <groupId>org.jetbrains.dokka</groupId>
                <artifactId>dokka-maven-plugin</artifactId>
                <version>${dokka.version}</version>
                <executions>
                    <execution>
                        <phase>pre-site</phase>
                        <goals>
                            <goal>javadocJar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- attach source -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>3.0.1</version>
                <executions>
                    <execution>
                        <id>attach-source</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- attach javadoc -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>2.10.4</version>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- BEGIN JUNIT TESTING -->
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.19.1</version>
                <configuration>
                    <includes>
                        <include>**/*Test*</include>
                    </includes>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>org.junit.platform</groupId>
                        <artifactId>junit-platform-surefire-provider</artifactId>
                        <version>${junit.platform.version}</version>
                    </dependency>
                    <dependency>
                        <groupId>org.junit.jupiter</groupId>
                        <artifactId>junit-jupiter-engine</artifactId>
                        <version>${jupiter.version}</version>
                    </dependency>
                </dependencies>
            </plugin>

            <!-- END JUNIT TESTING -->

            <!-- Make sure an up to date maven is used.
                 This is mostly due to changes to how version numbers are parsed in maven, starting with 3.2.0
                 -->
            <plugin>
                <inherited>true</inherited>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <version>1.3</version>
                <executions>
                    <execution>
                        <id>enforce-maven-3</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <requireMavenVersion>
                                    <version>3.2.0</version>
                                </requireMavenVersion>
                            </rules>
                            <fail>true</fail>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>