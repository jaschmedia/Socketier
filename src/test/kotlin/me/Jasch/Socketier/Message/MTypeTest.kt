package me.Jasch.Socketier.Message

import com.winterbe.expekt.should
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith


/**
 * @author jasch
 * @version 0.1.0
 * @since 0.1.0
 */
@RunWith(JUnitPlatform::class)
class MTypeTest : Spek({
    describe("Dealing with message types.") {
        given("The MType object") {
            it("should give a string equaling its own name") {
                MType.ACK.toString().should.equal("ACK")
                MType.NAC.toString().should.equal("NAC")
                MType.ATH.toString().should.equal("ATH")
                MType.ERR.toString().should.equal("ERR")
                MType.CIS.toString().should.equal("CIS")
                MType.PRT.toString().should.equal("PRT")
                MType.RTL.toString().should.equal("RTL")
                MType.PNG.toString().should.equal("PNG")
                MType.POG.toString().should.equal("POG")
                MType.EVT.toString().should.equal("EVT")
            }
        }

        given("a string representing a message type") {
            it("should return said message type") {
                MType.fromStr("ACK").should.equal(MType.ACK)
                MType.fromStr("NAC").should.equal(MType.NAC)
                MType.fromStr("ATH").should.equal(MType.ATH)
                MType.fromStr("ERR").should.equal(MType.ERR)
                MType.fromStr("CIS").should.equal(MType.CIS)
                MType.fromStr("PRT").should.equal(MType.PRT)
                MType.fromStr("RTL").should.equal(MType.RTL)
                MType.fromStr("PNG").should.equal(MType.PNG)
                MType.fromStr("POG").should.equal(MType.POG)
                MType.fromStr("EVT").should.equal(MType.EVT)
            }
        }

        given("A string not representing a valid message type") {
            it("should return null") {
                MType.fromStr("LOL").should.be.`null`
                MType.fromStr("").should.be.`null`
            }
        }
    }
})
/*
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import com.natpryce.hamkrest.nothing
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith


/**
 * @author jasch
 * @version 0.1.0
 * @since 0.1.0
 */
@RunWith(JUnitPlatform::class)
class MTypeTest : Spek ({
    describe("Possible types of messages.") {
        given("The MType object") {
            it("should give a string equaling its own name") {
                assertThat("ACK", MType.ACK.toString(), equalTo("ACK"))
                assertThat("NAC", MType.NAC.toString(), equalTo("NAC"))
                assertThat("ATH", MType.ATH.toString(), equalTo("ATH"))
                assertThat("ERR", MType.ERR.toString(), equalTo("ERR"))
                assertThat("CIS", MType.CIS.toString(), equalTo("CIS"))
                assertThat("PRT", MType.PRT.toString(), equalTo("PRT"))
                assertThat("RTL", MType.RTL.toString(), equalTo("RTL"))
                assertThat("PNG", MType.PNG.toString(), equalTo("PNG"))
                assertThat("POG", MType.POG.toString(), equalTo("POG"))
                assertThat("EVT", MType.EVT.toString(), equalTo("EVT"))
            }
        }

        given("The string representation of an MType") {
            it("should return the MType") {
                assertThat("ACK", MType.fromStr("ACK"), equalTo(MType.ACK))
                assertThat("NAC", MType.fromStr("NAC"), equalTo(MType.NAC))
                assertThat("ATH", MType.fromStr("ATH"), equalTo(MType.ATH))
                assertThat("ERR", MType.fromStr("ERR"), equalTo(MType.ERR))
                assertThat("CIS", MType.fromStr("CIS"), equalTo(MType.CIS))
                assertThat("PRT", MType.fromStr("PRT"), equalTo(MType.PRT))
                assertThat("RTL", MType.fromStr("RTL"), equalTo(MType.RTL))
                assertThat("PNG", MType.fromStr("PNG"), equalTo(MType.PNG))
                assertThat("POG", MType.fromStr("POG"), equalTo(MType.POG))
                assertThat("EVT", MType.fromStr("EVT"), equalTo(MType.EVT))
            }
        }
    }
})
*/
/*
class MTypeTest {
    @Test
    fun testDiscriminators() {
        assertEquals("ACK", "ACK", MType.ACK.toString())
        assertEquals("NAC", "NAC", MType.NAC.toString())
        assertEquals("ATH", "ATH", MType.ATH.toString())
        assertEquals("ERR", "ERR", MType.ERR.toString())
        assertEquals("CIS", "CIS", MType.CIS.toString())
        assertEquals("PRT", "PRT", MType.PRT.toString())
        assertEquals("RTL", "RTL", MType.RTL.toString())
        assertEquals("PNG", "PNG", MType.PNG.toString())
        assertEquals("POG", "POG", MType.POG.toString())
        assertEquals("EVT", "EVT", MType.EVT.toString())
    }

    @Test
    fun testStringToMType() {
        assertEquals("ACK", MType.ACK, MType.fromStr("ACK"))
        assertEquals("NAC", MType.NAC, MType.fromStr("NAC"))
        assertEquals("ATH", MType.ATH, MType.fromStr("ATH"))
        assertEquals("ERR", MType.ERR, MType.fromStr("ERR"))
        assertEquals("CIS", MType.CIS, MType.fromStr("CIS"))
        assertEquals("PRT", MType.PRT, MType.fromStr("PRT"))
        assertEquals("RTL", MType.RTL, MType.fromStr("RTL"))
        assertEquals("PNG", MType.PNG, MType.fromStr("PNG"))
        assertEquals("POG", MType.POG, MType.fromStr("POG"))
        assertEquals("EVT", MType.EVT, MType.fromStr("EVT"))
    }

    @Test
    fun testUnknownStringToMType() {
        assertEquals("LOL", null, MType.fromStr("LOL"))
        assertEquals("empty string", null, MType.fromStr(""))
    }
}
*/
