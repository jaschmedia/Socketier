package me.Jasch.Socketier.Message

import com.winterbe.expekt.should
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith


/**
 * @author jasch
 * @version 0.1.0
 * @since 0.1.0
 */
@RunWith(JUnitPlatform::class)
class MessageUtilsTest : Spek ({
    describe("Utility functions to extract information from a message string") {
        given("a well formed message string") {
            it("should extract the information correctly") {
                for (t in MType.values()) {
                    val testMsg = Message(t, "12345678", "foobarquoz").toString()
                    getMessageType(testMsg).should.equal(t)
                    getConnectionID(testMsg).should.equal("12345678")
                    getPayload(testMsg).should.equal("foobarquoz")
                }
            }
        }

        given("a malformed message string") {
            it("should fail to extract the message type") {
                val testStr = "ERT12345678"
                getMessageType(testStr).should.be.`null`
            }
            it("should fail to extract the connection ID") {
                val testStr = "ERR125678"
                getConnectionID(testStr).should.be.`null`
            }
            it("should fail to extract the payload") {
                val testStr = "ERT12345"
                getPayload(testStr).should.be.`null`
            }
        }
    }
})
/*
class MessageUtilsTest {
    @Test
    fun testMessageUtils() {
        for (t in MType.values()) {
            val testMsg = Message(t, "12345678", "foobarquoz").toString()
            assertEquals("MType $t", t, getMessageType(testMsg))
            assertEquals("CID", "12345678", getConnectionID(testMsg))
            assertEquals("Payload", "foobarquoz", getPayload(testMsg))
        }
    }
}
*/