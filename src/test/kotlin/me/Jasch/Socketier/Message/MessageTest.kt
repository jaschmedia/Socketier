package me.Jasch.Socketier.Message

import com.winterbe.expekt.should
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith


/**
 * @author jasch
 * @version 0.1.0
 * @since 0.1.0
 */
@RunWith(JUnitPlatform::class)
class MessageTest : Spek ({
    describe("Creating messages") {
        given("a set of MType, cid and no payload") {
            it("should create an appropriate message object") {
                for (t in MType.values()) {
                    val testMsg = Message(t, "12345678", "").toString()
                    Message.create(t, "12345678").toString().should.equal(testMsg)
                    Message.create("${t}12345678").toString().should.equal(testMsg)
                }
            }
        }

        given("a set of MType, cid and payload") {
            it("should create an appropriate message object") {
                for (t in MType.values()) {
                    val testMsg = Message(t, "12345678", "thePayload").toString()
                    Message.create("${t}12345678thePayload").toString().should.equal(testMsg)
                }
            }
        }
    }

    // TODO: more fuzzing of the message creation system.

})