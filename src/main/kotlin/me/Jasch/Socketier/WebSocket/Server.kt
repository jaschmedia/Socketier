package me.Jasch.Socketier.WebSocket

import me.Jasch.Socketier.Connection.Connection
import me.Jasch.Socketier.Connection.ConnectionState
import me.Jasch.Socketier.Event.EventHandler
import me.Jasch.Socketier.Message.MType
import me.Jasch.Socketier.Message.Message
import mu.KotlinLogging
import org.java_websocket.WebSocket
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.server.WebSocketServer
import java.lang.Exception
import java.net.InetSocketAddress

var conns = HashMap<String, Connection>()

/**
 * The WebSocket server.
 * @author jasch
 * @version 0.1.0
 * @since 0.1.0
 * @param webSocketAddress The address to listen to.
 * @param protocolName The name of the user protocol used.
 * @param eventHandler The event handler to use.
 */
class Server(
        webSocketAddress: InetSocketAddress,
        val protocolName: String,
        val eventHandler: EventHandler
) : WebSocketServer(webSocketAddress) {
    private val logger = KotlinLogging.logger {} // Logging

    override fun onOpen(conn: WebSocket?, handshake: ClientHandshake?) {
        if (conn == null) return
        val cid = generateConnectionID()
        val cn = Connection(cid, conn)
        conns.put(cid, cn)
        val msg = Message.create(MType.CIS, cid)
        conn.send(msg.toString())
        cn.state = ConnectionState.CIDSENT
        logger.debug { "Connection opened. Remote: ${conn.remoteSocketAddress.address.hostAddress}, CID: $cid" }
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onClose(conn: WebSocket?, code: Int, reason: String?, remote: Boolean) {
        logger.debug { "Connection closed. Remote: ${conn?.remoteSocketAddress?.address?.hostAddress ?: "unknown"}" }
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onMessage(conn: WebSocket?, message: String?) {
        if (message == null) {
            logger.error { "Received message without message!" }
            return
        }
        if (conn == null) {
            logger.error { "Received message from a non existent connection." }
            return
        }

        val msg: Message

        try {
            msg = Message.create(message)
        } catch (e: IllegalArgumentException) {
            // TODO: Do something
            logger.warn { "Received malformed message. ${e.message}" }
            return
        }

        //<editor-fold desc="log incoming messages">
        // lazy string to avoid unnecessary string creations during production.
        val logMsg: String by lazy { "${msg.mt} received. Remote: ${conn.remoteSocketAddress.address.hostAddress}, Message: $msg" }
        when (msg.mt) {
            MType.ERR -> logger.warn { logMsg }
            MType.CIS, MType.PRT, MType.EVT -> logger.debug { logMsg }
            else -> logger.info { logMsg }
        }
        //</editor-fold>

        when (msg.mt) {
            MType.ACK -> return
            MType.NAC -> return
            MType.ATH -> return
            MType.ERR -> {
                // close the connection and remove it from the list
                // TODO: Improve error handling
                conn.close()
                conns.remove(msg.cid)
            }
            MType.CIS -> {
                if (checkCISInformation(msg)) {
                    conns[msg.cid]?.state = ConnectionState.CIDACK
                    sendProtocolInformation(conn, msg.cid, protocolName)
                } else {
                    val sendMsg = Message.create(MType.ERR, msg.cid, "Failed to validate connection ID.")
                    conn.send(sendMsg.toString())
                    conn.close()
                    conns.remove(msg.cid)
                }
            }
            MType.PRT -> {
                if (checkProtocolInformation(msg, protocolName)) {
                    conns[msg.cid]?.state = ConnectionState.PRTACK
                } else {
                    val sendMsg = Message.create(MType.ERR, msg.cid, "Failed to validate protocol version.")
                    conn.send(sendMsg.toString())
                    conn.close()
                    conns.remove(msg.cid)
                }
            }
            MType.RTL -> return
            MType.PNG -> return
            MType.POG -> return
            MType.EVT -> eventHandler.dispatchEvent(msg)
        }

        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onError(conn: WebSocket?, ex: Exception?) {
        logger.warn { "A websocket error occurred. ${ex?.message}" }
        conn?.close()
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}