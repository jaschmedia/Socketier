package me.Jasch.Socketier.WebSocket

import me.Jasch.Socketier.Connection.Connection
import me.Jasch.Socketier.Connection.ConnectionState
import me.Jasch.Socketier.Message.MType
import me.Jasch.Socketier.Message.Message
import mu.KotlinLogging
import org.apache.commons.lang.RandomStringUtils
import org.java_websocket.WebSocket

/**
 * @author jasch
 * @version 0.1.0
 * @since 0.1.0
 */

private val logger = KotlinLogging.logger {} // Logging

/**
 * Generates random connection ID using the default connections collection.
 * @return The generated connection ID
 */
fun generateConnectionID(): String {
    return generateConnectionID(conns)
}

/**
 * Generates a random connection ID that isn't in the supplied connections collection.
 * @param connections The connections collection
 * @return The generated connection ID
 */
fun generateConnectionID(connections: HashMap<String, Connection>): String {
    var cid: String
    do {
        cid = RandomStringUtils.randomAlphanumeric(8)
    } while (connections.containsKey(cid))
    logger.debug { "Generated connection ID: $cid" }
    return cid
}

/**
 * Checks whether the message is a CIS for a connection that requires a CIS.
 * @param msg The message object
 * @return True if CIS for CIDSENT, otherwise false.
 */
fun checkCISInformation(msg: Message): Boolean = conns[msg.cid]?.state === ConnectionState.CIDSENT

/**
 * Checks whether a connection ID is in the connections collection.
 * @param cid The connection ID
 * @return True if found, false if not found or CID was null.
 */
fun knownCID(cid: String?): Boolean = conns.containsKey(cid) // can be String? because false is a wanted result

@Deprecated("Not used anymore. Use message object creation for this.")
fun terminateConnection(ws: WebSocket, cid: String) {
    val msg = Message.create(MType.ERR, cid, "Generic error")
    ws.send(msg.toString())
    ws.close()
    conns.remove(cid)
}

/**
 * Sends the protocol information to a connection and sets its state.
 * @param ws The affected WebSocket
 * @param cid The connection ID
 * @param protocolName The protocol name
 */
fun sendProtocolInformation(ws: WebSocket, cid: String, protocolName: String) {
    val msg = Message.create(MType.PRT, cid, protocolName)
    ws.send(msg.toString())
    conns[cid]?.state = ConnectionState.PRTSENT
}

/**
 * Checks whether the received PRT message is valid.
 * @param msg The received message
 * @param protocolName The protocol name
 * @return True if protocol name matches and connection was in PRTSENT state, otherwise false
 */
fun checkProtocolInformation(msg: Message, protocolName: String): Boolean {
    return conns[msg.cid]?.state === ConnectionState.PRTSENT && msg.payload == protocolName
}

/**
 * Sends a message object.
 * @param msg The message object.
 */
fun sendMessage(msg: Message) {
    conns[msg.cid]?.ws?.send(msg.toString())
}

// 😺
// Yes, just a random emoji comment because I just found out that IntelliJ 2017.1 now supports this!