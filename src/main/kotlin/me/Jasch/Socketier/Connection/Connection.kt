package me.Jasch.Socketier.Connection

import me.Jasch.Socketier.Message.Message
import org.java_websocket.WebSocket

/**
 * An object representing a connection.
 * @author jasch
 * @version 0.1.0
 * @since 0.1.0
 */
class Connection(
        val cid: String,
        val ws: WebSocket
) {
    var state = ConnectionState.NOTINIT

    /**
     * Sends a message over the WebSocket.
     * @param msg The message object.
     */
    fun send(msg: Message) = ws.send(msg.toString())
}