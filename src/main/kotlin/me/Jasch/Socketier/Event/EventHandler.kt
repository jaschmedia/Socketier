package me.Jasch.Socketier.Event

import me.Jasch.Socketier.Message.MType
import me.Jasch.Socketier.Message.Message
import me.Jasch.Socketier.WebSocket.knownCID
import mu.KotlinLogging

/**
 * The event handler.
 * @author jasch
 * @version 0.1.0
 * @since 0.1.0
 */
class EventHandler {
    private val logger = KotlinLogging.logger {}
    private var events: MutableMap<String, IEvent> = HashMap<String, IEvent>()

    /**
     * Register a new event.
     * @param id The identifier for the event.
     * @param event The event object.
     * @return True if successful, otherwise false.
     */
    @Suppress("unused") // This is part of the public API
    fun registerEvent(id: String, event: IEvent): Boolean {
        var success = false
        if (validEventID(id)) {
            if (!events.containsKey(id)) {
                events.put(id, event)
                success = true
            }
        }
        if (logger.isDebugEnabled) logger.debug("Registered event. ID: $id")
        return success
    }

    /**
     * Create the message for an event.
     * @param id The ID of the event.
     * @param cid The affected connection ID.
     * @param payload the payload to send.
     * @return The message object.
     */
    fun createEventMessage(id: String, cid: String, payload: Any): Message? {
        if (events.containsKey(id)) {
            val msg = Message.create(MType.EVT, cid, id + ":" + events.get(id)?.stringifyPayload(payload))
            if (logger.isDebugEnabled) logger.debug("Event created. ID: $id, CID: $cid")
            return msg
        } else {
            logger.info("")
            return null
        }
    }

    /**
     * Dispatches the event for a specific message.
     * @param msg The message
     */
    fun dispatchEvent(msg: Message) {

        val id = msg.payload.substringBefore(':')
        if (knownCID(msg.cid) && events.containsKey(id)) {
            events[id]?.handleEvent(msg)
        } else {
            logger.debug("Failed to dispatch event.")
        }

    }


    /**
     * Checks if ID string is alphanumeric
     * @param id The ID
     * @return True if alphanumeric, false otherwise.
     */
    private fun validEventID(id: String): Boolean = id.all(Char::isLetterOrDigit)
}