package me.Jasch.Socketier.Event

import me.Jasch.Socketier.Message.Message

/**
 * Interface for user events.
 * @author jasch
 * @version 0.1.0
 * @since 0.1.0
 */
interface IEvent {
    /**
     * Handle a message for this event.
     * @param msg The received message.
     */
    fun handleEvent(msg: Message)

    /**
     * Turn the payload for this event into a string.
     * @param payload The payload.
     */
    fun stringifyPayload(payload: Any): String
}