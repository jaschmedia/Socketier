package me.Jasch.Socketier.Message

/**
 * Possible types of messages sent.
 * @author jasch
 * @version 0.1.0
 * @since 0.1.0
 */
enum class MType {
    ACK, // acknowledge
    NAC, // not acknowledge

    ATH, // authenticate

    ERR, // Error

    CIS, // connection ID set
    PRT, // protocol information

    RTL, // rate limited - NOT IMPLEMENTED

    PNG, // ping - NOT IMPLEMENTED
    POG, // pong - NOT IMPLEMENTED
    EVT; // event

    companion object {
        private val map = MType.values().associateBy(MType::name)
        /**
         * Turns string representation into message type.
         * @param ms String representation
         * @return The message type or null if string doesn't represent a message type.
         */
        fun fromStr(ms: String): MType? = map[ms]
    }
}