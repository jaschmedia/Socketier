package me.Jasch.Socketier.Message

import mu.KotlinLogging

/**
 * A message object.
 * @author jasch
 * @version 0.1.0
 * @since 0.1.0
 * @param mt The message type of the message.
 * @param cid The connection ID of the message.
 * @param payload The payload of the message.
 */
class Message(
        val mt: MType,
        val cid: String,
        val payload: String
) {
    private val logger = KotlinLogging.logger {}

    init {
        logger.debug { "Message created. MType: $mt, CID: $cid, Payload: $payload" }
    }


    /**
     * Turns message into string representation.
     * @return The string.
     */
    override fun toString(): String {
        return mt.toString() + cid + payload
    }

    companion object Factory {
        /**
         * Creates message without payload.
         * @param mt The message type.
         * @param cid The connection ID.
         * @return The message object.
         */
        fun create(mt: MType, cid: String): Message = Message(mt, cid, "")

        /**
         * Creates a message.
         * @param mt The message type.
         * @param cid The connection ID.
         * @param payload The payload.
         * @return The message object.
         */
        fun create(mt: MType, cid: String, payload: String): Message = Message(mt, cid, payload)

        /**
         * Creates a message from a string representation.
         * @param msg The string representation
         * @return The message object.
         * @throws IllegalArgumentException If one of the message properties could not be extracted.
         */
        fun create(msg: String): Message {
            val mt = getMessageType(msg)
            val cid = getConnectionID(msg)
            val payload = getPayload(msg)
            if (mt == null) throw IllegalArgumentException("Invalid message type.")
            if (cid == null) throw IllegalArgumentException("Invalid connection ID.")
            if (payload == null) throw IllegalArgumentException("Invalid payload.")
            return Message(mt, cid, payload)
        }
    }
}
