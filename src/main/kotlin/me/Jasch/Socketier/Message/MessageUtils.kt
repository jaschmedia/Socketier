package me.Jasch.Socketier.Message

/**
 * @author jasch
 */

/**
 * Returns the message type of a message.
 * @param message The message string
 * @return The message type if found, otherwise null
 */
fun getMessageType(message: String): MType? {
    // TODO: Check implementation
    val mtString = message.subSequence(0,3)
    return MType.fromStr(mtString.toString())
}

/**
 * Returns the connection ID of a message.
 * @param message The message string
 * @return The connection ID if found, otherwise null
 */
fun getConnectionID(message: String): String? {
    // TODO: Check implementation
    try {
        return message.substring(3,11)

    } catch (e: Exception) {
        return null
    }
}

/**
 * Returns the payload of a message.
 * @param message The message string
 * @return The payload if found, otherwise null.
 */
fun getPayload(message: String): String? {
    // TODO: Implement
    try {
        return message.substring(11)
    } catch (e: Exception) {
        return null
    }

}