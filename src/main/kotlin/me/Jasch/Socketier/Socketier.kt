package me.Jasch.Socketier

import me.Jasch.Socketier.Event.EventHandler
import me.Jasch.Socketier.Message.Message
import me.Jasch.Socketier.WebSocket.Server
import me.Jasch.Socketier.WebSocket.sendMessage
import mu.KotlinLogging
import java.net.InetSocketAddress

/**
 * @author jasch
 * @version 0.1.0
 * @since 0.1.0
 */
class Socketier(val webSocketAddress: InetSocketAddress, val protocolName: String) : Thread() {
    private val logger = KotlinLogging.logger {}

    val eh = EventHandler()
    var ws: Server = Server(webSocketAddress, protocolName, eh)

    init {
        logger.info { "Socketier created. Address: $webSocketAddress, Protocol: $protocolName" }
    }

    override fun start() {
        ws.start()
    }

    fun getHandler(): EventHandler {
        return eh
    }

    fun sendEvent(msg: Message) {
        sendMessage(msg)
    }
}